TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := x64

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/x86_64-linux-musl-cross/bin/x86_64-linux-musl-

HOST_CPP = gcc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TOP_DIR)/trail/x64-generic/config/
